import React, { useEffect, useState } from "react"
import PropTypes from "prop-types"
import axios from 'axios';
import { Tabs, Tab, Box, Container, List, ListItem, ListItemIcon, Checkbox, ListItemText, Typography, ListItemSecondaryAction, IconButton, ButtonGroup } from "@material-ui/core"
import { Alert, AlertTitle } from '@material-ui/lab';
import RefreshIcon from '@material-ui/icons/Refresh';
import DeleteIcon from '@material-ui/icons/Delete';
import Layout from "../components/layout" 
import SEO from "../components/seo"
import Loader from "../components/loader";
import { CONFIG } from "..//constants/config"
import AddTodo from "../components/addTodo";
const TabPanel = ({ children, value, index, ...other }) => {
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          {children}
        </Box>
      )}
    </div>
  );
}
TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const IndexPage = () => {
  const API_URL = `${CONFIG.API_URL}/todo`;
  const [value, setValue] = useState(0);
  const [todos, setTodos] = useState({
    all: [],
    active: [],
    completed: [],
  });
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const [query, setQuery] = useState('');

  const {all, active, completed} = todos;
   
  useEffect(() => {
    fetchTodos();
  }, [query]);

  const onSearchInputChange = (newQuery) => {
    setQuery(newQuery);
  }

  
  const fetchTodos = async () => {
    try {
      setLoading(true);
      const response = await axios.get(`${API_URL}?q=${query}`);
      const allTodos = response.data;
      const activeTodos = allTodos.filter(todo => !todo.completed);
      const completedTodos = allTodos.filter(todo => todo.completed);
      setTodos({
        all: allTodos,
        active: activeTodos,
        completed: completedTodos,
      });
      setLoading(false);
      setError('');
    } catch (error) {
      setLoading(false);
      setError(error.message);
    }
  }
  const handleChange = (event, newValue) => setValue(newValue);
  const onCompleteItem = async (todoItem, index) => {
    await optimisticMoveTodoItems(todoItem, index, false);
  };
  const onUncompleteItem = async (todoItem, index) => {
    await optimisticMoveTodoItems(todoItem, index, true);
  };
  const optimisticMoveTodoItems = async (todoItem, index, alreadyCompleted) => {
    const {active: savedActive, complted: savedCompleted} = todos;
    const newTodo = {
      ...todoItem, 
      completed: !todoItem.completed
    };
    setTodos({
      all, 
      active: alreadyCompleted
        ? [newTodo, ...active]
        : active.filter((todoItem, i) => index !== i),
      completed: !alreadyCompleted 
        ? [newTodo, ...completed]
        : completed.filter((todoItem, i) => index !== i),
    });
    try {
      await changeTodoItemCompletion(newTodo);
    } catch (error) {
      // obsluga jakiegos komunikatu? 
      setTodos({
        all,
        active: savedActive,
        completed: savedCompleted,
      })
    }
  }
  const changeTodoItemCompletion = async (todoItem) => {
    const urlPostFix = todoItem.completed ? 'complete': 'uncomplete';
    await axios.put(`${API_URL}/${todoItem.id}/${urlPostFix}`);
  }
  
  
  const onDelete = async (todoItem) => {
    await axios.delete(`${API_URL}/${todoItem.id}`);
    await fetchTodos();
  }
  
  return (
    <Layout onSearchInputChange={onSearchInputChange}>
      <SEO title="Home" />
      <Container>
        <Box pt={3}>
          <Typography variant="h2">Lista zadań</Typography>
        </Box>
        
        <AddTodo
          onAdd={fetchTodos}
        />
        
        <Loader
          visible={loading}
          darkMode={true}
          opacity={0.5}
          size={120}
          color={'secondary'}
        />
        { error !== '' && (
          <Alert 
              severity="error"
              action={
                <Box padding={3}>
                  <ButtonGroup>
                    <IconButton 
                      edge="end" 
                      aria-label="refresh"
                      onClick={() => fetchTodos()}
                    >
                      <RefreshIcon />
                    </IconButton>
                    <IconButton 
                      edge="end" 
                      aria-label="delete"
                      onClick={() => setError('')}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </ButtonGroup>
                </Box>
              }
            >
            <AlertTitle>Błąd</AlertTitle>
            <strong>{ error }</strong>
          </Alert> 
        )}
        {all.length > 0 && (
          <>
            <div>
              <Tabs 
                indicatorColor="secondary"
                textColor="primary"
                centered
                value={value} 
                onChange={handleChange} 
                aria-label="simple tabs example"
              >
                <Tab label="Do zrobienia" id="simple-tab-0" aria-controls="simple-tabpanel-0" />
                <Tab label="Ukończone" id="simple-tab-1" aria-controls="simple-tabpanel-1" />
              </Tabs>
            </div>
            <TabPanel value={value} index={0}>
              <List>
                {active.map((todoItem, key) => (
                  <ListItem key={key}>
                  <ListItemIcon>
                      <Checkbox
                        edge="start"
                        checked={todoItem.completed}
                        tabIndex={-1}
                        disableRipple
                        inputProps={{ 'aria-labelledby': key }}
                        onClick={() => onCompleteItem(todoItem, key)}
                      />
                    </ListItemIcon>
                    <ListItemText>{todoItem.title}</ListItemText>
                    <ListItemText>{todoItem.description}</ListItemText>
                    <ListItemSecondaryAction>
                      <IconButton 
                        edge="end" 
                        aria-label="delete"
                        onClick={() => onDelete(todoItem)}>
                        <DeleteIcon />
                      </IconButton>
                    </ListItemSecondaryAction>
                  </ListItem>
                ))}       
              </List>
            </TabPanel>
            <TabPanel value={value} index={1}>
            <List>
              {completed.map((todoItem, key) => (
                <ListItem key={key}>
                <ListItemIcon>
                    <Checkbox
                      edge="start"
                      checked={todoItem.completed}
                      tabIndex={-1}
                      disableRipple
                      inputProps={{ 'aria-labelledby': key }}
                      onClick={() => onUncompleteItem(todoItem, key)}
                    />
                  </ListItemIcon>
                  <ListItemText>{todoItem.title}</ListItemText>
                  <ListItemText>{todoItem.description}</ListItemText>
                  <ListItemSecondaryAction>
                    <IconButton 
                      edge="end" 
                      aria-label="delete" 
                      onClick={() => onDelete(todoItem)}>
                      <DeleteIcon />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
              ))}       
            </List>
          </TabPanel>
          </> 
        )}
      </Container>
    </Layout>
  )
}
export default IndexPage