import React, { useState } from 'react';
import styled from 'styled-components';
import axios from 'axios';
import { TextField, Checkbox, FormControlLabel, Button } from '@material-ui/core';
import { CONFIG } from '../constants/config';

const StyledWrapper = styled.form`
    padding: 30px;
    margin-bottom: 50px;
    button {
        float: right;
    }
`

const initialFormValues = {
    title: '',
    description: '', 
    completed: false
};

const AddTodo = ({ onAdd }) => {
    const [formValues, setFormValues] = useState(initialFormValues);

    const onSubmit = async (event) => {
        event.preventDefault();
        await axios.post(`${CONFIG.API_URL}/todo`, formValues);
        setFormValues(initialFormValues);
        onAdd();
    }

    // DO OBSLUGI FORMULARZY NA PRZYSZLOSC
    const onChange = name => event => {
        setFormValues({
            ...formValues,
            [name]: event.target.value,
        });
    }
    
    const onToggleCompleted = () => {
        setFormValues({
            ...formValues,
            completed: !formValues.completed,
        });
    }

    const {title, description, completed} = formValues;

    return (
        <StyledWrapper onSubmit={onSubmit}>
            <TextField 
                label="Tytuł"
                fullWidth
                variant="outlined"
                margin="normal"
                value={title}
                onChange={onChange('title')}
                required
            />
            <TextField 
                label="Opis"
                fullWidth
                variant="outlined"
                margin="normal"
                value={description}
                onChange={onChange('description')}
            />
            <div>
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={completed}
                            onChange={onToggleCompleted}
                            color="primary"
                        />
                    }
                    label="Ukończone"                
                />
            </div>
            <Button type="submit" variant="contained" color="primary">
                Dodaj
            </Button>            
        </StyledWrapper>
    )
}


export default AddTodo;