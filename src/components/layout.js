import React from "react"
import PropTypes from "prop-types"
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { lime, orange, teal } from '@material-ui/core/colors';
import Header from "./header"
import "./layout.css"
const theme = createMuiTheme({
  palette: {
    primary: teal,
    secondary: lime,
  },  
  status: {
    danger: lime[500],
  },
});;
const Layout = ({ children, onSearchInputChange }) => {
  
  return (
    <ThemeProvider theme={theme}>
      <Header 
        siteTitle="Lista zadań" 
        onSearchInputChange={onSearchInputChange}
      />
      <main>{children}</main>        
    </ThemeProvider>
  )
}
Layout.propTypes = {
  children: PropTypes.node.isRequired,
  onSearchInputChange: PropTypes.func,
}

export default Layout