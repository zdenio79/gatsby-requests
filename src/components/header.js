import { AppBar, IconButton, Toolbar, Typography, InputBase } from "@material-ui/core"
import { Link } from "gatsby"
import MenuIcon from "@material-ui/icons/menu"
import styled from 'styled-components'
import SearchIcon from "@material-ui/icons/search"
import PropTypes from "prop-types"
import React, { useEffect } from "react"
import { useState } from "react"
import {useDebounce} from "../hooks/useDebounce"

const StyledRoot = styled.div`
  flex-grow: 1;
`;
const StyledMenuButton = styled(IconButton)`
  margin-right: 20px;
`;
const StyledToolbar = styled(Toolbar)` 
  display: flex;
  justify-content: space-between;
  /* background-color: red; */
`;
const StyledToolbarLeft = styled.div`
  display: flex;
  align-items: center;
`;
const StyledSearchWrapper = styled.div`
  padding: 0 10px;
  position: 'relative';
  border-radius: 20;
  display: flex;
  align-items: center;
  background-color: rgba(255,255,255, 0.15);
  margin-left: 0;
  width: '100%';
  &:hover {
    background-color: rgba(255,255,255, 0.25);
  };
`;
const StyledSearchIcon = styled(SearchIcon)`
  padding: 0 3px;
  height: '100%';
  pointer-events: 'none';
  display: 'flex';
  align-items: 'center';
  justify-content: 'center';
`;
const StyledInputBase = styled(InputBase)` 
  color: inherit;
  input {
    padding: 5px;
    transition: width 0.4s ease;
    width: '100%',
  }
`;
const Header = ({ siteTitle, onSearchInputChange }) => {
  const [query, setQuery] = useState('');
  const debounceQuery = useDebounce (query, 500)
  
  useEffect(() => {
    onSearchInputChange(debounceQuery);
  }, [debounceQuery]);

  return (
  <StyledRoot>
    <AppBar position="static" color="primary">
      <StyledToolbar>
        <StyledToolbarLeft>
          <StyledMenuButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
          >
            <MenuIcon />
          </StyledMenuButton>
          <Typography variant="h6" noWrap>
            { siteTitle }
          </Typography>
        </StyledToolbarLeft>
        <StyledSearchWrapper>
          <StyledSearchIcon />
          <StyledInputBase 
            placeholder="Wyszukaj"
            inputProps={{ 'aria-label': 'search' }}
            value={query}
            onChange={(event) => setQuery(event.target.value)}
          />
        </StyledSearchWrapper>
      </StyledToolbar>
    </AppBar>
  </StyledRoot>
  )
 }


Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
